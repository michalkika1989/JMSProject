package bean.session;

import java.util.List;
import javax.ejb.Local;
import model.Message;

/**
 * Local bean for db actions
 * @author Michal
 */
@Local
public interface MessageSessionBeanLocal {    
    /**
     * Save message object to DB 
     * @param msg 
     */
    void saveMessage(Message msg);
    
    /**
     * Return list of all messages stored in DB
     * @return list 
     */
    List<Message> getMessages();    
}
