package bean.session;

import javax.ejb.Local;

/**
 * Local bean for send message to jms queue
 * @author Michal
 */

@Local
public interface SenderBeanLocal {
    
    /**
     * Send message to jms queue
     * @param message 
     */
    void sendMessage(String message);
}
