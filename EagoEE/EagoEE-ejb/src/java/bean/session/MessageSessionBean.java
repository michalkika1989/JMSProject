package bean.session;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.PersistenceContext;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import model.Message;

/**
 * Implementation of db actions
 * @author Michal
 */
@Stateless
public class MessageSessionBean implements MessageSessionBeanLocal {

    private String NAMED_QUERY_FINDALL = "Message.findAll";
    
    @PersistenceContext (name="EagoPU")
    private EntityManager em;
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
    @Override
    public void saveMessage(Message msg){
        try{
            em.persist(msg);        
        }catch (EntityExistsException | IllegalArgumentException | TransactionRequiredException e){
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    @Override
    public List<Message> getMessages() {
        try{
            return em.createNamedQuery(NAMED_QUERY_FINDALL).getResultList();
        }catch(IllegalArgumentException | QueryTimeoutException | TransactionRequiredException |
               PessimisticLockException | LockTimeoutException e){
            
            logger.log(Level.WARNING, e.getMessage());
        }
        return new ArrayList<>();
    }   
}


