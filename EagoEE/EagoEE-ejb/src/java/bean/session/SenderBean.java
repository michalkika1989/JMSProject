package bean.session;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * Implementation of sender
 * @author Michal
 */
@Stateless
public class SenderBean implements SenderBeanLocal{
 
    @Resource(mappedName="myQueueConFac1")    
    private ConnectionFactory connectionFactory;

    @Resource(mappedName="myQueue2")  
    private Queue queue;
    
    Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void sendMessage(String message) {
        try (Connection connection = connectionFactory.createConnection()){            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(queue);
            producer.send(session.createTextMessage(message));
            logger.log(Level.INFO, "Reply to client sent: {0}", message);
        }catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }       
    }
}
