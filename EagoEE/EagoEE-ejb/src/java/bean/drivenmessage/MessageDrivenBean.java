package bean.drivenmessage;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import bean.session.MessageSessionBeanLocal;
import bean.session.SenderBeanLocal;

/**
 * Bean is receiving messages from jms queue
 * @author Michal
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "myQueue1"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MessageDrivenBean implements MessageListener {

    @EJB
    private MessageSessionBeanLocal messageSessionBean;
    
    @EJB
    private SenderBeanLocal sender;
    
    Logger logger = Logger.getLogger(this.getClass().getName());
        
    protected MessageDrivenBean() {
    }
    
    @Override
    public void onMessage(Message message) {
         try{  
            TextMessage txt = (TextMessage)message; 
            logger.log(Level.INFO, "Following message income from client: {0}", txt.getText());
            
            model.Message dbMess = new model.Message();
            dbMess.setMessage(txt.getText());
            dbMess.setTimestamp(new Date());
            
            messageSessionBean.saveMessage(dbMess);
            
            logger.log(Level.INFO, "Message saved to db: {0}", dbMess); 
            
            sender.sendMessage(dbMess.getTimestamp().toString());

        }catch(JMSException e){
            logger.log(Level.SEVERE, null, e);
        }  
    }    
}
