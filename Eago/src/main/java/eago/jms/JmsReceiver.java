package eago.jms;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Class is receiving messages by listener from jms queue
 * @author Michal
 */

public class JmsReceiver implements MessageListener{
    
    /**
     * Const for connection factory name in glassfish
     */
    private final String QUEUE_CON_FAC_NAME = "myQueueConFac1";
    
    /**
     * Const for queue name in glassfish
     */
    private final String QUEUE_NAME = "myQueue2";
    
    private Logger logger = Logger.getLogger(this.getClass().getName());
    
    private InitialContext          context                 = null;
    private QueueConnectionFactory  queueConnectionFactory  = null;
    private QueueConnection         queueConnection         = null;
    private QueueSession            queueSession            = null;
    private Queue                   queue                   = null;
    private QueueReceiver           queueReceiver           = null;
    
    public JmsReceiver(){     

    }     
    
    /**
     * Start receiving from jms queue
     */
    public void startReceiving(){
         try{  
            context = new InitialContext();  
            queueConnectionFactory = (QueueConnectionFactory)context.lookup(QUEUE_CON_FAC_NAME);  
            queueConnection = queueConnectionFactory.createQueueConnection(); 
            
            queueConnection.start();  
            queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);  
            queue = (Queue)context.lookup(QUEUE_NAME);  
            queueReceiver = queueSession.createReceiver(queue);            
            
            queueReceiver.setMessageListener(this);             
            
            logger.log(Level.INFO, "Receiving started from server");            
            
        }catch(NamingException | JMSException e){
            logger.log(Level.SEVERE, e.getMessage());
        }finally{
             stopReceiving();
         }
    }
    
    /**
     * Stop receiving
     */
    private void stopReceiving(){
            try {
            if(queueReceiver != null){
                queueReceiver.close();
            }
            if(queueSession != null){
                queueSession.close();
            }
            if(queueConnection != null){
                queueConnection.close();
            }
            logger.log(Level.INFO, "Receiving stopped");            
        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }        
    }  

    /**
     * Listener
     * @param message 
     */
    @Override
    public void onMessage(Message message) {
        try {
            TextMessage msg=(TextMessage)message;  
            logger.log(Level.INFO, "Following message income from server: {0}", msg.getText());
        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }  
}

