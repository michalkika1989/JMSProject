package eago.jms;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.*;  
import javax.jms.*;  
  
/**
 * Class is sending messages to jms queue
 * @author Michal
 */
public class JmsSender {  
    
    /**
     * Const for connection factory name in glassfish
     */
    private final String QUEUE_CON_FAC_NAME = "myQueueConFac1";
    
    /**
     * Const for queue name in glassfish
     */
    private final String QUEUE_NAME = "myQueue1";
    
    private Logger logger = Logger.getLogger(this.getClass().getName());
    
    private InitialContext          context                 = null;
    private QueueConnectionFactory  queueConnectionFactory  = null;
    private QueueConnection         queueConnection         = null;
    private QueueSession            queueSession            = null;
    private Queue                   queue                   = null;
    private QueueSender             queueSender             = null;
    private TextMessage             textMessage             = null;
    
    public JmsSender() {  
        
    }  
    
    /**
     * Send message to jsm queue
     * @param message 
     */
    public void send (String message){
        try  
        {   
            context = new InitialContext();  
            queueConnectionFactory = (QueueConnectionFactory)context.lookup(QUEUE_CON_FAC_NAME);  
            queueConnection = queueConnectionFactory.createQueueConnection(); 
            
            queueConnection.start();  
            queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);  
            queue = (Queue)context.lookup(QUEUE_NAME);  
            queueSender = queueSession.createSender(queue);  
            textMessage = queueSession.createTextMessage();                
            textMessage.setText(message);  
            queueSender.send(textMessage);  
            
            closeConnection();
            
            logger.log(Level.INFO, "Message sent to server: {0}", message);
            
        }catch(NamingException | JMSException e){
            logger.log(Level.SEVERE, e.getMessage());
        }finally {
            closeConnection();
        }
    }
    
    /**
     * Close opened connection to jms queue
     */
    private void closeConnection(){
        try {
            if(queueSender != null){
                queueSender.close();
            }
            if(queueSession != null){
                queueSession.close();
            }
            if(queueConnection != null){
                queueConnection.close();
            }
            logger.log(Level.INFO, "Connection closed");            
        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }     
    }           
}  