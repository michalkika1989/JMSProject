package eago.wicket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

/**
 * Create main web application
 * @author Michal
 */
public class WicketApplication extends WebApplication{
	
        /**
         * Initialize home page
         * @return 
         */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return HomePage.class;
	}
	
	@Override
	public void init()
	{
		super.init();
                getRequestCycleSettings().setResponseRequestEncoding("UTF-8"); 
                getMarkupSettings().setDefaultMarkupEncoding("UTF-8");     
               
	}
}
