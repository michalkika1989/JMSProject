package eago.wicket;

import eago.jms.JmsReceiver;
import eago.jms.JmsSender;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

public class HomePage extends WebPage { 
    
        /**
         * Web page container ids
         */
        private final String FEEDBACKPANEL_FEEDBACK = "feedback";
        private final String TEXTFIELD_MESSAGE      = "message";
        private final String FORM_USER              = "userForm";
        private final String LABEL_REPLY            = "reply";
        
	public HomePage(final PageParameters parameters) {
            super(parameters);     
            
            JmsReceiver jmsReceiver = new JmsReceiver();
            jmsReceiver.startReceiving();
            
            Label reply = new Label(LABEL_REPLY, "Server reply: none");           
            add(reply);                    
            
            final FeedbackPanel feedback = new FeedbackPanel(FEEDBACKPANEL_FEEDBACK);            
            add(feedback);             
            
            final TextField<String> message = new TextField<>(TEXTFIELD_MESSAGE, Model.of(""));
            message.setRequired(true);            
            
            final Form<?> form;
            form = new Form<Void>(FORM_USER) {
                
                @Override
                protected void onSubmit() {          
                    
                    final String mes = message.getModelObject();
                    message.setModel(Model.of(""));                        
                        
                    JmsSender sender = new JmsSender();
                    sender.send(mes); 
                                      
                }
            };

            add(form);
            form.add(message);

        }  
}
